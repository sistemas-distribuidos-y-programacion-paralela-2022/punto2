FROM openjdk:11-jre-slim-bullseye
COPY target/tp2-punto2-a-1.0-SNAPSHOT.jar usr/src/tp2-punto2-a-1.0-SNAPSHOT.jar
COPY src/cuenta.txt usr/src/src/cuenta.txt
WORKDIR /usr/src
EXPOSE 9090
ENTRYPOINT [ "java", "-jar", "tp2-punto2-a-1.0-SNAPSHOT.jar" ]
CMD "java -jar tp2-punto2-a-1.0-SNAPSHOT.jar"