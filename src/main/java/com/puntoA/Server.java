package com.puntoA;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import org.apache.logging.log4j.*;

public class Server {

    private static Logger logger = LogManager.getLogger(Server.class.getName());


    public Server(int port) {
        /*
        Capa de Trasnporte: TCP / UDP
         */
        try{
            ServerSocket ss = new ServerSocket(port);
            System.out.println("Server escuchando en el puerto " + port);
            logger.info("Server escuchando conexiones TCP en el puerto "+ port);
            while (true){
                Socket client = ss.accept();
                logger.info("Atendiendo al cliente "+ client);
                ServerHilo sh = new ServerHilo(client);
                Thread serverThread = new Thread(sh);
                serverThread.start();

            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }
    public static void main(String[] args) {
        int port = Integer.valueOf(System.getenv("PUERTO_SERVIDOR"));     
        Server server = new Server(port);
    }
}
