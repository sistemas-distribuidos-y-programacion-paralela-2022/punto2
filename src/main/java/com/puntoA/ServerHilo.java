package com.puntoA;

import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.Scanner;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import org.apache.logging.log4j.*;

public class ServerHilo implements Runnable {
    Socket client;
    Gson gson = new Gson();
    private static Logger logger = LogManager.getLogger(ServerHilo.class.getName());

    public ServerHilo(Socket client) {
        this.client = client;

    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        try {

            PrintWriter canalSalida = new PrintWriter(client.getOutputStream(), true);
            BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
            String solicitudJson = br.readLine();
            System.err.println("Mensaje recibido: " + solicitudJson);
            logger.info("Mensaje recibido " + solicitudJson);
            Message solicitud = gson.fromJson(solicitudJson, Message.class);
            logger.info("HEADER: " + solicitud.getHeader());
            String respuesta;
            if (solicitud.getHeader().startsWith("putDeposito")) {

                Message respuestaMovimiento = actualizarMonto(Integer.valueOf(solicitud.bodyJSON), "putDeposito", 4000);
                respuesta = gson.toJson(respuestaMovimiento);
                canalSalida.println(respuesta);

            }
            if (solicitud.getHeader().startsWith("putExtraccion")) {

                Message respuestaMovimiento = actualizarMonto(Integer.valueOf(solicitud.bodyJSON), "putExtraccion",
                        8000);
                respuesta = gson.toJson(respuestaMovimiento);
                canalSalida.println(respuesta);

            }
            canalSalida.flush();
            br.close();
            canalSalida.close();

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private Message actualizarMonto(int monto, String movimiento, int tiempo) {
        String mensaje = "";
        Message mensajeRespuesta;
        boolean b = false;
        String data = "0";

        try {
            File file = new File("src/cuenta.txt");
            Scanner myReader = new Scanner(file);

            while (myReader.hasNextLine()) {
                data = myReader.nextLine();
                System.out.println(data);
            }
            logger.info("Saldo en la cuenta: " + data);
            if (Integer.valueOf(data) >= 0) {
                switch (movimiento) {
                    case "putExtraccion":
                        if (Integer.valueOf(data) > 0 && monto <= Integer.valueOf(data)) {
                            monto = Integer.valueOf(data) - monto;
                            // System.out.println("Saldo actual: " + monto);
                            myReader.close();
                            mensaje = "200 OK";
                            b = true;
                            data = String.valueOf(monto);
                            logger.info("Extraccion");
                            logger.info("Saldo actualizado: " + data);
                        } else {
                            mensaje = "500 Error al extraer";
                            logger.info("Error al extraer " + monto);
                            logger.info("Saldo actual: " + data);
                        }
                        break;
                    case "putDeposito":
                        monto = Integer.valueOf(data) + monto;
                        System.out.println("MONTO actual: " + monto);
                        myReader.close();
                        mensaje = "200 OK";
                        data = String.valueOf(monto);
                        b = true;
                        logger.info("Deposito");
                        logger.info("Saldo actualizado: " + data);
                        break;
                    default:
                        break;
                }
                if (b) {
                    try {
                        FileWriter myWriter = new FileWriter(file);
                        Thread.sleep(tiempo);
                        myWriter.write(String.valueOf(monto));
                        myWriter.close();
                    } catch (Exception e) {
                        System.err.println(e.getMessage());
                        mensaje = "500 Error";
                        logger.error("Error al guardar el fichero");
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            logger.error(e.getMessage());
            mensaje = "500 Error";
        }

        return mensajeRespuesta = new Message(mensaje, data);
    }

}
