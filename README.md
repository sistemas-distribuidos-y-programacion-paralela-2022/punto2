# punto2

mvn clean package; docker build . -t williamsoft/tp2-p2a-servidor; docker push williamsoft/tp2-p2a-servidor


## Instrucciones

El ejercicio se encuentra desplegado en el cluster provisto por la asignatura.

Para cambiar entre el punto 2.a y 2.b hay que modificar la variable de entorno del deployment del cliente y elegir el deployment del servidor deseado.